package comments

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"gitflic.ru/project/rustore/rustoreapi/client"
	"gitflic.ru/project/rustore/rustoreapi/errors"
)

const (
	GetCommentsFullURL         = client.BaseURL + "/%s/comment"
	AnswerCommentURL           = client.BaseURL + "/%s/feedback?commentId=%s"
	RedactOrDeleteCommentURL   = client.BaseURL + "/%s/feedback/%s"
	GetCommentsFeedbackFullURL = client.BaseURL + "/%s/feedback"
	GetCommentsFullCSVURL      = client.BaseURL + "/%s/comment/export?from=%s&to=%s"
	GetRaitingsURL             = client.BaseURL + "/%s/comment/statistic"
)

type Comments struct {
	client      *client.Client
	packageName string
}

func New(
	c *client.Client,
	packageName string,
) *Comments {

	return &Comments{
		client:      c,
		packageName: packageName,
	}
}

// nolint: tagliatelle
type GetCommentsFullResponse struct {
	Code                string    `json:"code"`
	Message             *string   `json:"message"`
	GetCommentsFullBody []Comment `json:"body"`
	Timestamp           time.Time `json:"timestamp"`
}

// nolint: tagliatelle
type Comment struct {
	PackageName    string `json:"packageName"`
	AppID          int    `json:"appId"`
	CommentID      int64  `json:"commentId"`
	UserName       string `json:"userName"`
	AppRating      int    `json:"appRating"`
	CommentStatus  string `json:"commentStatus"`
	CommentDate    string `json:"commentDate"`
	CommentText    string `json:"commentText"`
	LikeCounter    int    `json:"likeCounter"`
	DislikeCounter int    `json:"dislikeCounter"`
	UpdatedAt      string `json:"updatedAt"`
	AppVersionName string `json:"appVersionName"`
	Edited         bool   `json:"edited"`
}

func (c *Comments) GetCommentsFull(
	ctx context.Context,
	params url.Values,
) (
	GetCommentsFullResponse,
	error,
) {
	url := fmt.Sprintf(
		GetCommentsFullURL,
		c.packageName,
	)

	if len(params) > 0 {
		url += "?" + params.Encode()
	}

	response, err := c.client.DoRequest(ctx, http.MethodGet, url, nil)
	if err != nil {
		return GetCommentsFullResponse{}, fmt.Errorf(errors.ErrSendingRequest, "GetCommentsFull", err)
	}

	var getCommentsFull GetCommentsFullResponse

	err = json.NewDecoder(response).Decode(&getCommentsFull)
	if err != nil {
		return GetCommentsFullResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "GetCommentsFull", err)
	}

	defer response.Close()

	return getCommentsFull, nil
}

type AnswerCommentResponse struct {
	Code      string            `json:"code"`
	Message   *string           `json:"message"`
	Body      AnswerCommentBody `json:"body"`
	Timestamp time.Time         `json:"timestamp"`
}

type AnswerCommentBody struct {
	ID int `json:"id"`
}

func (c *Comments) AnswerComment(
	ctx context.Context,
	commentID string,
	feedbackText string,
) (
	AnswerCommentResponse,
	error,
) {
	feedbackTextJSON, err := json.Marshal(FeedbackText{Message: feedbackText})
	if err != nil {
		return AnswerCommentResponse{}, fmt.Errorf(errors.ErrMarshalling, "AnswerComment", err)
	}

	url := fmt.Sprintf(
		AnswerCommentURL,
		c.packageName,
		commentID,
	)

	response, err := c.client.DoRequest(ctx, http.MethodPost, url, feedbackTextJSON)
	if err != nil {
		return AnswerCommentResponse{}, fmt.Errorf(errors.ErrSendingRequest, "AnswerComment", err)
	}

	var answerComment AnswerCommentResponse

	err = json.NewDecoder(response).Decode(&answerComment)
	if err != nil {
		return AnswerCommentResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "AnswerComment", err)
	}

	defer response.Close()

	return answerComment, nil
}

type FeedbackText struct {
	Message string `json:"message"`
}

type RedactFeedbackResponse struct {
	Code      string             `json:"code"`
	Message   *string            `json:"message"`
	Body      RedactFeedbackBody `json:"body"`
	Timestamp time.Time          `json:"timestamp"`
}

type RedactFeedbackBody struct {
	ID int64 `json:"id"`
}

func (c *Comments) RedactFeedback(
	ctx context.Context,
	feedbackID string,
	feedbackText string,
) (
	RedactFeedbackResponse,
	error,
) {
	feedbackTextJSON, err := json.Marshal(FeedbackText{Message: feedbackText})
	if err != nil {
		return RedactFeedbackResponse{}, err
	}

	url := fmt.Sprintf(
		RedactOrDeleteCommentURL,
		c.packageName,
		feedbackID,
	)

	response, err := c.client.DoRequest(ctx, http.MethodPost, url, feedbackTextJSON)
	if err != nil {
		return RedactFeedbackResponse{}, fmt.Errorf(errors.ErrSendingRequest, "RedactFeedback", err)
	}

	var redactFeedback RedactFeedbackResponse

	err = json.NewDecoder(response).Decode(&redactFeedback)
	if err != nil {
		return RedactFeedbackResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "RedactFeedback", err)
	}

	defer response.Close()

	return redactFeedback, nil
}

type DeleteFeedbackResponse struct {
	Code      string             `json:"code"`
	Message   *string            `json:"message"`
	Body      DeleteFeedbackBody `json:"body"`
	Timestamp time.Time          `json:"timestamp"`
}

type DeleteFeedbackBody struct {
	ID int64 `json:"id"`
}

func (c *Comments) DeleteFeedback(
	ctx context.Context,
	feedbackID string,
) (
	DeleteFeedbackResponse,
	error,
) {

	url := fmt.Sprintf(
		RedactOrDeleteCommentURL,
		c.packageName,
		feedbackID,
	)

	response, err := c.client.DoRequest(ctx, http.MethodDelete, url, nil)
	if err != nil {
		return DeleteFeedbackResponse{}, fmt.Errorf(errors.ErrSendingRequest, "DeleteFeedback", err)
	}

	var deleteFeedback DeleteFeedbackResponse

	err = json.NewDecoder(response).Decode(&deleteFeedback)
	if err != nil {
		return DeleteFeedbackResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "DeleteFeedback", err)
	}

	defer response.Close()

	return deleteFeedback, nil
}

// nolint: tagliatelle
type GetRaitingsResponse struct {
	Code            string       `json:"code"`
	Message         *string      `json:"message"`
	GetRaitingsBody RaitingsBody `json:"body"`
	Timestamp       time.Time    `json:"timestamp"`
}

// nolint: tagliatelle
type RaitingsBody struct {
	GetRaitingsBodyRatings RaitingsBodyRatings `json:"ratings"`
	AverageUserRating      float64             `json:"averageUserRating"`
	TotalRatings           int                 `json:"totalRatings"`
	TotalResponses         int                 `json:"totalResponses"`
	RatingsNoComments      int                 `json:"ratingsNoComments"`
}

// nolint: tagliatelle
type RaitingsBodyRatings struct {
	AmountFive  int `json:"amountFive"`
	AmountFour  int `json:"amountFour"`
	AmountThree int `json:"amountThree"`
	AmountTwo   int `json:"amountTwo"`
	AmountOne   int `json:"amountOne"`
}

func (c *Comments) GetRaitings(
	ctx context.Context,
) (
	GetRaitingsResponse,
	error,
) {

	url := fmt.Sprintf(
		GetRaitingsURL,
		c.packageName,
	)

	response, err := c.client.DoRequest(ctx, http.MethodGet, url, nil)
	if err != nil {
		return GetRaitingsResponse{}, fmt.Errorf(errors.ErrSendingRequest, "GetRaitings", err)
	}

	var raitings GetRaitingsResponse

	err = json.NewDecoder(response).Decode(&raitings)

	if err != nil {
		return GetRaitingsResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "GetRaitings", err)
	}

	defer response.Close()

	return raitings, nil
}

type GetCommentFeedbackResponse struct {
	Code      string         `json:"code"`
	Message   *string        `json:"message"`
	Body      []FeedbackBody `json:"body"`
	Timestamp time.Time      `json:"timestamp"`
}

// nolint: tagliatelle
type FeedbackBody struct {
	ID        string    `json:"id"`
	CommentID string    `json:"commentId"`
	Text      string    `json:"text"`
	Status    string    `json:"status"`
	Date      time.Time `json:"date"`
}

func (c *Comments) GetCommentFeedbackStatesFull(
	ctx context.Context,
	params url.Values,
) (
	GetCommentFeedbackResponse,
	error,
) {

	url := fmt.Sprintf(
		GetCommentsFeedbackFullURL,
		c.packageName,
	)
	if len(params) > 0 {
		url += "?" + params.Encode()
	}

	response, err := c.client.DoRequest(ctx, http.MethodGet, url, nil)
	if err != nil {
		return GetCommentFeedbackResponse{},
			fmt.Errorf(
				errors.ErrSendingRequest,
				"GetCommentFeedbackStatesFull",
				err,
			)
	}

	var commentFeedback GetCommentFeedbackResponse

	err = json.NewDecoder(response).Decode(&commentFeedback)
	if err != nil {
		return GetCommentFeedbackResponse{},
			fmt.Errorf(errors.ErrDecodingJSON, "GetCommentFeedbackStatesFull", err)
	}

	defer response.Close()

	return commentFeedback, nil
}

func (c *Comments) GetCommentsCSVFile(
	ctx context.Context,
	dateFrom,
	dateTo string,
) (
	[]byte,
	error,
) {

	url := fmt.Sprintf(
		GetCommentsFullCSVURL,
		c.packageName,
		dateFrom,
		dateTo,
	)

	response, err := c.client.DoRequest(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrSendingRequest, "GetCommentsCSVFile", err)
	}

	data, err := io.ReadAll(response)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrReadingBody, "GetCommentsCSVFile", err)
	}

	defer response.Close()

	return data, nil
}
