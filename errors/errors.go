package errors

var (
	ErrMarshalling      = "error marshalling %s: %w"
	ErrUnmarshalling    = "error unmarshalling %s: %w"
	ErrSendingRequest   = "error sending request in %s: %w"
	ErrUploadingFile    = "error uploading file in %s: %w"
	ErrCreatingFormFile = "error creating form file in %s: %w"
	ErrOpeningFile      = "error opening file in %s: %w"
	ErrCopying          = "error copying in %s: %w"
	ErrFormingRequest   = "error forming request in %s: %w"
	ErrReadingBody      = "error reading body in %s: %w"
	ErrDecodingJSON     = "error decoding JSON in %s: %w"
)
