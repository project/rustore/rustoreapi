package client

import (
	"bytes"
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"time"

	"gitflic.ru/project/rustore/rustoreapi/errors"
)

const (
	BaseURL        = "https://public-api.rustore.ru/public/v1/application"
	AuthURL        = "https://public-api.rustore.ru/public/auth"
	timeOutSeconds = 5
)

type JWETokenResponse struct {
	Code    string   `json:"code"`
	Message string   `json:"message"`
	Body    JWEToken `json:"body"`
}

type JWEToken struct {
	JWE string `json:"jwe"`
}

type Client struct {
	companyID          string
	privateKeyRaw      string
	privateKeyComplete string
	keyID              string
	jwe                string
	httpClient         *http.Client
	timeout            time.Duration
}

type Option func(*Client)

func WithTimeout(timeOut time.Duration) Option {
	return func(c *Client) {
		c.timeout = timeOut
	}
}

func WithHTTTPClient(httpClient *http.Client) Option {
	return func(c *Client) {
		c.httpClient = httpClient
	}
}

func New(
	keyID,
	privateKeyRaw,
	companyID string,
	options ...Option,
) *Client {

	c := &Client{
		companyID:     companyID,
		privateKeyRaw: privateKeyRaw,
		httpClient:    &http.Client{},
		keyID:         keyID,
		timeout:       time.Second * timeOutSeconds,
	}

	for _, o := range options {
		o(c)
	}

	return c

} // создание нового клиента

func (c *Client) Auth() error {
	privateKeyComplete, err := c.GetEncodedSignature(
		c.keyID,
		c.privateKeyRaw,
	)
	if err != nil {
		return fmt.Errorf("error getting encoded signature: %w", err)
	}

	c.privateKeyComplete = privateKeyComplete // получили зашифрованный приватный ключ

	ctx, cancel := context.WithTimeout(
		context.Background(),
		timeOutSeconds*time.Second,
	)

	defer cancel()

	c.jwe, err = c.GetJWEToken(ctx, c.privateKeyComplete)
	if err != nil {
		return fmt.Errorf(errors.ErrSendingRequest, "GetJWEToken", err)
	}

	return nil
}

func (c *Client) GetEncodedSignature(
	keyID,
	privateKeyRaw string,
) (
	string,
	error,
) {

	completePrivateKey := fmt.Sprintf(
		"-----BEGIN PRIVATE KEY-----\n%s\n-----END PRIVATE KEY-----",
		privateKeyRaw,
	)

	timestamp := time.Now().Format(time.RFC3339Nano)

	block, _ := pem.Decode([]byte(completePrivateKey))
	if block == nil {
		return "", fmt.Errorf("error decoding key: check private key")
	}

	privateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return "", fmt.Errorf("error parsing private key: %w", err)
	}

	rsaPrivateKey, ok := privateKey.(*rsa.PrivateKey)
	if !ok {
		return "", fmt.Errorf("error reading private key")
	}

	messageToSign := keyID + timestamp // Отсюда возьмём хеш

	hashed := sha512.Sum512([]byte(messageToSign))

	signatureBytes, err := rsa.SignPKCS1v15(
		rand.Reader,
		rsaPrivateKey,
		crypto.SHA512,
		hashed[:],
	)
	if err != nil {
		return "", fmt.Errorf("error signing the key: %w", err)
	}

	signatureValue := base64.StdEncoding.EncodeToString(signatureBytes)

	resultMap := map[string]string{
		"keyId":     keyID,
		"signature": signatureValue,
		"timestamp": timestamp,
	}

	resultJSON, err := json.Marshal(
		resultMap,
	)
	if err != nil {
		return "", fmt.Errorf(errors.ErrMarshalling, "GetEncodedSignature", err)
	}

	return string(resultJSON), nil
}

func (c *Client) GetJWEToken(
	ctx context.Context,
	privateKeyComplete string,
) (
	string,
	error,
) {

	request, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		AuthURL,
		bytes.NewBuffer([]byte(privateKeyComplete)),
	)
	if err != nil {
		return "", fmt.Errorf(errors.ErrFormingRequest, "GetJWEToken", err)
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")

	response, err := c.httpClient.Do(request)
	if err != nil {
		return "", fmt.Errorf(errors.ErrSendingRequest, "GetJWEToken", err)
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return "", fmt.Errorf(errors.ErrReadingBody, "GetJWEToken", err)
	}

	jweToken := JWETokenResponse{}

	err = json.Unmarshal(body, &jweToken)
	if err != nil {
		return "", fmt.Errorf(errors.ErrUnmarshalling, "GetJWEToken", err)
	}

	return jweToken.Body.JWE, nil

}

type RequestOpts struct {
	CustomContentType string
}

func (c *Client) do(
	request *http.Request,
	opts RequestOpts,
) (
	*http.Response,
	error,
) {
	contentType := opts.CustomContentType
	if contentType == "" {
		contentType = "application/json; charset=UTF-8"
	}

	request.Header.Set("Content-Type", contentType)
	request.Header.Set("Public-Token", c.jwe)
	response, err := c.httpClient.Do(request)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrSendingRequest, "do", err)
	}

	return response, nil
}

func (c *Client) DoRequest(
	ctx context.Context,
	method string,
	url string,
	sendData []byte,
) (
	io.ReadCloser,
	error,
) {

	ctx, cancel := context.WithTimeout(ctx, c.timeout)

	defer cancel()

	request, err := http.NewRequestWithContext(
		ctx,
		method,
		url,
		bytes.NewBuffer(sendData),
	)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrFormingRequest, "DoRequest", err)
	}

	response, err := c.do(request, RequestOpts{})
	if err != nil {
		return nil, fmt.Errorf(errors.ErrSendingRequest, "DoRequest", err)
	}

	return response.Body, nil

}

func (c *Client) UploadFile(
	ctx context.Context,
	url string,
	fileName string,
) (
	io.ReadCloser,
	error,
) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	fileWriter, err := bodyWriter.CreateFormFile("file", fileName)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrCreatingFormFile, "UploadFile", err)
	}

	fh, err := os.Open(fileName)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrOpeningFile, "UploadFile", err)
	}

	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrCopying, "UploadFile", err)
	}

	contentType := bodyWriter.FormDataContentType()

	bodyWriter.Close()

	request, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		url,
		bodyBuf,
	)
	if err != nil {
		return nil, fmt.Errorf(errors.ErrFormingRequest, "UploadFile", err)
	}

	response, err := c.do(request, RequestOpts{contentType})
	if err != nil {
		return nil, fmt.Errorf(errors.ErrSendingRequest, "UploadFile", err)
	}

	return response.Body, nil
}
