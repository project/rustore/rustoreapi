package publishing

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"gitflic.ru/project/rustore/rustoreapi/client"
	"gitflic.ru/project/rustore/rustoreapi/errors"
)

const (
	versionURL               = client.BaseURL + "/%s/version"
	versionDeleteURL         = client.BaseURL + "/%s/version/%d"
	apkFileUploadURL         = client.BaseURL + "/%s/version/%d/apk?isMainApk=%t"
	aabFileUploadURL         = client.BaseURL + "/%s/version/%d/aab"
	iconFileUploadURL        = client.BaseURL + "/%s/version/%d/image/icon"
	screenFileUploadURL      = client.BaseURL + "/%s/version/%d/image/screenshot/%s/%s"
	changePublishSettingsURL = client.BaseURL + "/%s/version/%d/publish-settings"
	sendToModerationURL      = client.BaseURL + "/%s/version/%d/commit"
	manualPublishURL         = client.BaseURL + "/%s/version/%d/publish"
)

type Version struct {
	client      *client.Client
	packageName string
}

// nolint: tagliatelle
type CreateDraftText struct {
	AppName          string `json:"appName,omitempty"`
	AppType          string `json:"appType,omitempty"`
	Categories       string `json:"categories,omitempty"`
	AgeLegal         string `json:"ageLegal,omitempty"`
	ShortDescription string `json:"shortDescription,omitempty"`
	FullDescription  string `json:"fullDescription,omitempty"`
	WhatsNew         string `json:"whatsNew,omitempty"`
	ModerInfo        string `json:"moderInfo,omitempty"`
	PriceValue       string `json:"priceValue,omitempty"`
	PublishType      string `json:"publishType,omitempty"`
	PublishDateTime  string `json:"publishDateTime,omitempty"`
	PartialValue     string `json:"partialValue,omitempty"`
}

func New(c *client.Client, packageName string) *Version {
	return &Version{
		client:      c,
		packageName: packageName,
	}
}

// nolint: tagliatelle
type CreateVersionDraftResponse struct {
	Code      string    `json:"code"`
	Message   *string   `json:"message"`
	Body      int       `json:"body"`
	Timestamp time.Time `json:"timestamp"`
}

func (c *Version) CreateVersionDraft(
	ctx context.Context,
	draftText CreateDraftText,
) (
	CreateVersionDraftResponse,
	error,
) {
	sendData, err := json.Marshal(draftText)
	if err != nil {
		return CreateVersionDraftResponse{}, err
	}

	url := fmt.Sprintf(
		versionURL,
		c.packageName,
	)

	response, err := c.client.DoRequest(ctx, http.MethodPost, url, sendData)
	if err != nil {
		return CreateVersionDraftResponse{}, fmt.Errorf(errors.ErrSendingRequest, "CreateVersion", err)
	}

	var createVersionDraft CreateVersionDraftResponse

	err = json.NewDecoder(response).Decode(&createVersionDraft)
	if err != nil {
		return CreateVersionDraftResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "CreateVersion", err)
	}

	defer response.Close()

	return createVersionDraft, nil
}

type DeleteVersionDraftResponse struct {
	Code      string    `json:"code"`
	Message   *string   `json:"message"` // Message приходит только в случае ошибки
	Timestamp time.Time `json:"timestamp"`
}

func (c *Version) DeleteVersionDraft(
	ctx context.Context,
	versionID int,
) (
	DeleteVersionDraftResponse,
	error,
) {
	url := fmt.Sprintf(
		versionDeleteURL,
		c.packageName,
		versionID,
	)

	response, err := c.client.DoRequest(ctx, http.MethodDelete, url, nil)
	if err != nil {
		return DeleteVersionDraftResponse{}, fmt.Errorf(errors.ErrSendingRequest, "DeleteVersion", err)
	}

	var deleteVersionDraft DeleteVersionDraftResponse

	err = json.NewDecoder(response).Decode(&deleteVersionDraft)
	if err != nil {
		return DeleteVersionDraftResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "DeleteVersion", err)
	}

	defer response.Close()

	return deleteVersionDraft, nil
}

// nolint: tagliatelle
type GetVersion struct {
	Code      string         `json:"code"`
	Message   *string        `json:"message"`
	Body      GetVersionBody `json:"body"`
	Timestamp time.Time      `json:"timestamp"`
}

// nolint: tagliatelle
type GetVersionBody struct {
	Content       []Content `json:"content"`
	PageNumber    int       `json:"pageNumber"`
	PageSize      int       `json:"pageSize"`
	TotalElements int       `json:"totalElements"`
	TotalPages    int       `json:"totalPages"`
}

// nolint: tagliatelle
type Content struct {
	VersionID        int        `json:"versionId"`
	AppName          string     `json:"appName"`
	AppType          string     `json:"appType"`
	VersionName      *string    `json:"versionName"`
	VersionCode      int        `json:"versionCode"`
	VersionStatus    string     `json:"versionStatus"`
	PublishType      string     `json:"publishType"`
	PublishDateTime  *time.Time `json:"publishDateTime"`
	SendDateForModer *time.Time `json:"sendDateForModer"`
	PartialValue     int        `json:"partialValue"`
	WhatsNew         *string    `json:"whatsNew"`
	PriceValue       int        `json:"priceValue"`
	Paid             bool       `json:"paid"`
}

func (c *Version) GetVersion(
	ctx context.Context,
	params url.Values,
) (
	GetVersion,
	error,
) {
	url := fmt.Sprintf(
		versionURL,
		c.packageName,
	)

	if len(params) > 0 {
		url += "?" + params.Encode()
	}

	response, err := c.client.DoRequest(ctx, http.MethodGet, url, nil)
	if err != nil {
		return GetVersion{}, fmt.Errorf(errors.ErrSendingRequest, "GetVersion", err)
	}

	var getVersion GetVersion

	err = json.NewDecoder(response).Decode(&getVersion)
	if err != nil {
		return GetVersion{}, fmt.Errorf(errors.ErrDecodingJSON, "GetVersion", err)
	}

	defer response.Close()

	return getVersion, nil
}

type UploadFile struct {
	Code      string    `json:"code"`
	Message   *string   `json:"message"` // Message приходит только в случае ошибки
	Timestamp time.Time `json:"timestamp"`
}

func (c *Version) UploadAPKFile(
	ctx context.Context,
	versionID int,
	fileName string,
	isMainAPK bool,
	params url.Values,
) (
	UploadFile,
	error,
) {
	url := fmt.Sprintf(
		apkFileUploadURL,
		c.packageName,
		versionID,
		isMainAPK,
	)

	if len(params) > 0 {
		url += "?" + params.Encode()
	}

	response, err := c.client.UploadFile(ctx, url, fileName)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrUploadingFile, "UploadAPKFile", err)
	}

	var uploadAPKFile UploadFile

	err = json.NewDecoder(response).Decode(&uploadAPKFile)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrDecodingJSON, "UploadAPKFile", err)
	}

	defer response.Close()

	return uploadAPKFile, nil
}

func (c *Version) UploadAABFile(
	ctx context.Context,
	versionID int,
	fileName string,
) (
	UploadFile,
	error,
) {
	url := fmt.Sprintf(
		aabFileUploadURL,
		c.packageName,
		versionID,
	)

	response, err := c.client.UploadFile(ctx, url, fileName)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrUploadingFile, "UploadAABFile", err)
	}

	var uploadAABFile UploadFile

	err = json.NewDecoder(response).Decode(&uploadAABFile)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrDecodingJSON, "UploadAABFile", err)
	}

	defer response.Close()

	return uploadAABFile, nil
}

func (c *Version) UploadIcon(
	ctx context.Context,
	versionID int,
	fileName string,
) (
	UploadFile,
	error,
) {
	url := fmt.Sprintf(
		iconFileUploadURL,
		c.packageName,
		versionID,
	)

	response, err := c.client.UploadFile(ctx, url, fileName)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrUploadingFile, "UploadIcon", err)
	}

	var uploadIcon UploadFile

	err = json.NewDecoder(response).Decode(&uploadIcon)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrDecodingJSON, "UploadIcon", err)
	}

	defer response.Close()

	return uploadIcon, nil
}

func (c *Version) UploadScreenshot(
	ctx context.Context,
	versionID int,
	fileName string,
	orientation string,
	ordinal string,
) (
	UploadFile,
	error,
) {
	url := fmt.Sprintf(
		screenFileUploadURL,
		c.packageName,
		versionID,
		orientation,
		ordinal,
	)

	response, err := c.client.UploadFile(ctx, url, fileName)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrUploadingFile, "UploadScreenshot", err)
	}

	var uploadScreenshot UploadFile

	err = json.NewDecoder(response).Decode(&uploadScreenshot)
	if err != nil {
		return UploadFile{}, fmt.Errorf(errors.ErrDecodingJSON, "UploadFile", err)
	}

	defer response.Close()

	return uploadScreenshot, nil
}

type PublishResponse struct {
	Code      string    `json:"code"`
	Message   *string   `json:"message"`
	Timestamp time.Time `json:"timestamp"`
}

// nolint: tagliatelle
type CreatePublishSettingsText struct {
	PublishType     string `json:"publishType,omitempty"`
	PublishDateTime string `json:"publishDateTime,omitempty"`
	PartialValue    string `json:"partialValue,omitempty"`
}

func (c *Version) ChangePublishSettings(
	ctx context.Context,
	versionID int,
	publishSettingsText CreatePublishSettingsText,
) (
	PublishResponse,
	error,
) {
	publishSettingsTextJSON, err := json.Marshal(publishSettingsText)
	if err != nil {
		return PublishResponse{}, fmt.Errorf(errors.ErrMarshalling, "publishSettingsTextJSON", err)
	}

	url := fmt.Sprintf(
		changePublishSettingsURL,
		c.packageName,
		versionID,
	)

	response, err := c.client.DoRequest(ctx, http.MethodPost, url, publishSettingsTextJSON)
	if err != nil {
		return PublishResponse{}, fmt.Errorf(errors.ErrSendingRequest, "ChangePublishSettings", err)
	}

	var changePublishSettingsURL PublishResponse

	err = json.NewDecoder(response).Decode(&changePublishSettingsURL)
	if err != nil {
		return PublishResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "ChangePublishSettings", err)
	}

	defer response.Close()

	return changePublishSettingsURL, nil
}

func (c *Version) SendToModeration(
	ctx context.Context,
	versionID int,
	params url.Values,
) (
	PublishResponse,
	error,
) {
	url := fmt.Sprintf(
		sendToModerationURL,
		c.packageName,
		versionID,
	)

	if len(params) > 0 {
		url += "?" + params.Encode()
	}

	response, err := c.client.DoRequest(ctx, http.MethodPost, url, nil)
	if err != nil {
		return PublishResponse{}, fmt.Errorf(errors.ErrSendingRequest, "SendToModeration", err)
	}

	var sendToModeration PublishResponse

	err = json.NewDecoder(response).Decode(&sendToModeration)
	if err != nil {
		return PublishResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "SendToModeration", err)
	}

	defer response.Close()

	return sendToModeration, nil
}

func (c *Version) ManualPublish(
	ctx context.Context,
	versionID int,
) (
	PublishResponse,
	error,
) {
	url := fmt.Sprintf(
		manualPublishURL,
		c.packageName,
		versionID,
	)

	response, err := c.client.DoRequest(ctx, http.MethodPost, url, nil)
	if err != nil {
		return PublishResponse{}, fmt.Errorf(errors.ErrSendingRequest, "manualPublish", err)
	}

	var manualPublish PublishResponse

	err = json.NewDecoder(response).Decode(&manualPublish)
	if err != nil {
		return PublishResponse{}, fmt.Errorf(errors.ErrDecodingJSON, "ManualPublish", err)
	}

	defer response.Close()

	return manualPublish, nil
}
